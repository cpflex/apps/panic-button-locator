echo off
if [%1]==[] goto usage

REM Specifications
set ocmfile="panic-button-locator.ocm.json"
set uripkg=//hub.cpflex.tech/cora/interfaces/ocmv1/panic-button-locator
set tmpdir="ocmtemp"

REM Script compiles latest OCM Spec and pushes it to the hub.
set tag=%1
set uritag=%uripkg%:%tag%

echo ** Creating temp directory: %tmpdir%
mkdir %tmpdir%
xcopy *.ocm.json %tmpdir% /Y
xcopy schema  %tmpdir%\schema /I /Y
xcopy ..\..\v1\lib\ocm-schemas\*.ocm.json %tmpdir%\schema /Y
cd %tmpdir%

echo ** Compiling OCM Spec %ocmfile%
call ocmc compile %ocmfile% 

echo ** Updating Project Nid definitions.
copy /Y *.i ..\..\src

echo ** Pushing spec to CP-Flex Hub:  %uritag%
cphub push application/x-xmf specfile.ocm.xmf %uritag% -v
cphub push application/json  spec_%ocmfile% %uritag% -v

cd ..
rmdir /S /Q %tmpdir%

echo ** Publishing Complete.
goto :eof

:usage
@echo Usage: %0 ^<api tag (e.g. v1.0.2.3)^>
exit /B 1
