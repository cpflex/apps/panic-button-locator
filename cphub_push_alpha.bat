@ECHO OFF
if [%1]==[] goto usage

set specfile="v1_spec.json"
set tag=%1
ECHO Panic Button Locator Application version %1
cphub push -v -s %specfile% cpflexapp "./panic-button-locator.bin" cora/apps/misc/panic-button-locator:%tag%
goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
