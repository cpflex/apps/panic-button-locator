﻿# Panic Button Locator Application Release Notes

### V1.001 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.
### V1.000a - 230811
  1. Firmware Reference  v2.2.1.0
  2. Initial Implementation
