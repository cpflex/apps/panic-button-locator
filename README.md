# Panic Button Locator Application
**Version: 1.001**<br/>
**Release Date: 2023/09/19**

This application provides a simple panic button with location capability designed for maximum battery life.

## Key Features  
This application configures the CT1000 to provide a simple panic button interface.  All location features are disabled to
preserve battery life except when the duress mode is activated.

Key features include:
1. 1.5 second "press and hold" to activate panic button alert.
2. Caching of messages when out of range
3. Battery status events and report
4. Health Check two (2) times per day.
5. 1 minute location reporting interval when in duress mode.

# User Interface 
This section describes the essential functions of the CT1000 user interface as defined
by this application.   

## Button Commands  

The following tables summarize the button activated commands organized into primary operational and additional commands.  
### Operational Commands 
These commands are intended for operational use, providing a very simple user interface.
| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Duress Activation**               | hold > 1.5 seconds            | any button or both | Activates Duress mode.    |  

### Additional Commands
For testing and device management, the following commands are available but not required
for ordinary operational use.
| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Duress Cancel (demo only)**       | five (5) quick presses | Button #1 | Deactivates Duress mode.  Only available in demonstration mode  |  
| **Battery Status**                  | Three (3) <br>quick presses       | Button #2           | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator) 
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads 

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **Duress Mode Activating**     |  Red blink <br>ten (10) times quickly       |    both     |
| **Duress Mode Active**         |  Two (2) red blink once every 5 seconds     |    both     |
| **Duress Mode Deactivating**   |  Green blink <br>ten (10) times quickly     |    both     |
| **Device Reset**               |       Green blink <br>three (3) times       |    both     |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |    LED #1   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |    LED #1   |

## Duress Mode 

Duress mode provides thes panic button feature, where when the user presses any button for more than 1.5 seconds the duress mode is activated.  Once activated it sends an notification message to the cloud. 
## Battery Indicator 

Activating the battery status commend will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description|
|----------|-------------|------------|
|  1 red   |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

## Settings ##

### Cloud configurable non-volatile Settings
The application supports non-volatile downlink settings for standard reporting interval.  

### Network Link Check Settings
Network link Checks are performed using a combination of time and activity metrics.  Uplink checks are performed
in accordance to the following schedule.   If confirmed messages are sent, the schedule is updated.

|  Link Check Metric                 |      Value            |
|:-----------------------------------|:---------------------:|
|   Stationary Time Interval         |   every 10 minutes    |
|   Stationary Uplink Activity       |   every 3 messages    |
|   Active Tracking Time Interval    |   every 10 minutes    |
|   Active Tracking Uplink Activity  |   every 3 messages    |


These settings can be adjusted by implementing a custom ConfigLinkCheck.i

## Network status ##
When the tag leaves network coverage area, it will try to connect to the network when there is a message needs to be uploaded to the cloud. The tag will save its scanning data when it is out of the network. The saved location data will be send to the cloud when the tag comes back to the network area. If the tag is not moving, it will check the network status according to loRaWAN standard.


  These values survive reboot or dead battery conditions.


---
*Copyright 2023, Codepoint Technologies, Inc.* <br>
*All Rights Reserved*
