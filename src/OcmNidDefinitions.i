/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* ODS Panic Button Locator Schema
*  nid:        PanicButton
*  uuid:       1185faf0-10c1-4360-9812-03b90a82f00d
*  ver:        1.0.0.0
*  date:       2023-08-12T00:05:12.952Z
*  product: Cora Tracking CT100X
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_SystemLogAll = 2,
	NID_LocConfigPmodeDefault = 3,
	NID_TrackConfig = 4,
	NID_SystemLogDisable = 5,
	NID_PowerBattery = 6,
	NID_LocConfigPtech = 7,
	NID_SystemReset = 8,
	NID_PowerChargerCritical = 9,
	NID_TrackConfigNomintvl = 10,
	NID_SystemConfigPollintvl = 11,
	NID_SystemLog = 12,
	NID_TrackMode = 13,
	NID_SystemLogAlert = 14,
	NID_LocConfigPmodePrecise = 15,
	NID_LocConfigPtechBle = 16,
	NID_LocConfigPtechWifi = 17,
	NID_TrackModeActive = 18,
	NID_LocConfig = 19,
	NID_LocConfigPmodeMedium = 20,
	NID_PowerCharger = 21,
	NID_LocConfigPmodeCoarse = 22,
	NID_TrackModeDisabled = 23,
	NID_TrackConfigEmrintvl = 24,
	NID_SystemErase = 25,
	NID_LocConfigPtechWifible = 26,
	NID_LocConfigPtechAutomatic = 27,
	NID_PowerChargerCharging = 28,
	NID_SystemLogInfo = 29,
	NID_TrackConfigAcquire = 30,
	NID_TrackConfigInactivity = 31,
	NID_SystemLogDetail = 32,
	NID_PowerChargerCharged = 33,
	NID_SystemConfig = 34,
	NID_PbDuressActivated = 35,
	NID_PbDuress = 36,
	NID_PbDuressDeactivated = 37,
	NID_PowerChargerDischarging = 38,
	NID_TrackModeEnabled = 39
};
