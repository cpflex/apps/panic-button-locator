/**
 *  Name:  app.p
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019-2023 Codepoint Technologies
 *  All Rights Reserved
 */

#include "app.i"
#include "version.i" //Compile time generated file.

//System includes.
#include "cpflexver.i"


//Modules
#include "battery.i"
#include "TelemMgr.i"
#include "SysMgr.i"
#include "MotionTracking.i"
#include "StatefulEvent.i"

//Include application configuration data.
#include "config/ConfigApp.i"


/*******************************************************
* Forward Declarations.
*******************************************************/
forward @IndicateDuress( tick, epoch);


/*******************************************************
* System events.
*******************************************************/
@SysInit()
{
	Log(   "*********** Panic Button Locator **************",	MC_info, MP_med, LO_default);
	LogFmt("** Version:      %s",								MC_info, MP_med, LO_default, __VERSION__);
	LogFmt("** CP-Flex Ver:  %s",								MC_info, MP_med, LO_default, CPFLEX_VERSION);
	LogFmt("** Build Date:   %s %s",							MC_info, MP_med, LO_default, __DATE__, __TIME__);
	Log(   "***********************************************",	MC_info, MP_med, LO_default);

	#ifdef __SYSLOG__
		//Test mode we automatically enable the System Log.
		SysSetLogFilters( 0xFF, 0xFF);
	#endif 

	trk_Init();
	trk_ReportZeroMeasurements(true); //MBM 12/14/2021 

	battery_Init();
	TelemMgr_Init();
	SysmgrInit();
	StatefulEventInit();
}

/*******************************************************
* Handle Events.
*******************************************************/
@UiButtonEvent( idButton, ButtonPress: press, ctClicks)
{
	TraceDebugFmt( "Button: id=%d, press=%d, clicks=%d", idButton, _: press, ctClicks);
	switch( press) {
		case BP_long:  	   { StatefulEventSet(EVENT_DURESS, SE_SET, true);} 
		case BP_verylong:  { StatefulEventSet(EVENT_DURESS, SE_SET, true);} 
		case BP_multi: {
			switch( ctClicks) {
				case 3:  {if( idButton==BTN2){ battery_IndicateChargeLevel();}}
				case 5:  {if( idButton==BTN1){ StatefulEventSet(EVENT_DURESS, SE_CLEAR, true);}}
			}
		}
	}
}


/**
* @brief Provides periodic indicator for emergency mode
*/

@IndicateDuress( tick, epoch) 
{
	//Signal user Duress Mode is active.
	UiSetLed( LED2, LM_count, 2, 157, 156);
	UiSetLed( LED4, LM_count, 2, 157, 156);
}

#pragma warning disable 203
/**
* @brief Power state change has occured.
*/
stock app_EnterPowerSaving(pctCharged)
{
	// Tracking is deactivated to preserve battery.	
	TraceDebug("Entering Power Save Mode")
	//Nothing to do.
}

stock app_ExitPowerSaving(pctCharged)
{
	TraceDebug("Exiting Power Save Mode")
	//Nothing to do.
}
#pragma warning enable 203

/***
* @brief Callback event handler implemented by the application to receive
*   notification of event state changes.
* @param idEvent The stateful event identifier.
* @param newState  The new state identifier.
*/
stock  app_StatefulEventCallback(  idEvent, StatefulEventState: newState) {
	switch(idEvent) {
		case EVENT_DURESS: {
			if( newState == SE_SET) {
				TraceDebug("*** Duress Activated");
				UiSetLed( LED2, LM_count, 10, 157, 156);
				UiSetLed( LED4, LM_count, 10, 157, 156);
				SetRtcAlarm( Now() + 5, 5, TIMER_INFINITE_CYCLES, "@IndicateDuress");

				//Set emergency reporting and force tracking to activate
				trk_SetInterval(true);
				trk_Activate();
				trk_Acquire();
			} else if( newState == SE_CLEAR) {
				//Turn off Tracking.
				trk_Deactivate();

				//Flash both green for three seconds.
				UiSetLed( LED1, LM_count, 10, 157, 156);
				UiSetLed( LED3, LM_count, 10, 157, 156);
				KillRtcAlarm("@IndicateDuress" );
				TraceDebug("*** Duress Deactivated");
			}
		}
	}
}

/*******************************************************
* Communication Implementation
*******************************************************/

/**
* @brief Received data sequence handler.
* @remarks.  Processes received sequences and reports any requested. data.
*/
@TelemRecvSeq( Sequence: seqIn, ctMsgs )
{
	//Process the received sequence.
	new Message: msg;
	new MsgType: tmsg;
	new tcode;
	new id;
	new MsgPriority: priority;
	new MsgCategory: category;
	new ResultCode: rc;
	new int: ct= int: 0;
	new Sequence:seqOut;

	//Allocate a sequence to send messages.
	//Create a sequence to hold all potential commands.
	rc = TelemSendSeq_Begin( seqOut, 150); 
	if( rc != RC_success)
	{
		TraceError("Could not allocate sequence buffer, ");
	}
	
	//Process each of the received messages.
	while( rc == RC_success 
		&& TelemRecv_NextMsg( seqIn, msg, tmsg, tcode, id, priority, category) 
			== RC_success)
	{
		//Process simple messages first. These are commands to respond with 
		//some report.
		if( tmsg == MT_Int32Msg)
		{
			switch( id) 
			{
				case  NM_CMD_TRACKING:	 	{rc = trk_ProcessCommand(seqOut, msg); ++ct;  }
				case  NM_CMD_CONFIG: 	 	{rc = trk_ReportSettings(seqOut); ++ct;  }
				case  NM_CMD_POWER_STATUS: 	{rc = battery_ReportStatus( seqOut); ++ct;  }
				case  NM_CMD_BATTERY_LEVEL: {rc = battery_ReportChargeLevel( seqOut); ++ct;  }
			}
		}
		//Next check to see if we received a settings message.
		else if( tmsg == MT_KeyValueMsg && id == NM_CMD_CONFIG )
		{
			trk_UpdateSettings(msg);
		}
				
	 	//Process using Module Handlers.
		ct += SysmgrDownlink( seqOut, tmsg, id, msg);
		ct += TelemMgr_Downlink( seqOut, tmsg, id, msg);
		ct += StatefulEventDownlink( seqOut, tmsg, id, msg);		
	}

	//Done with the input sequence.
	TelemRecv_SeqEnd( seqIn);

	//Either send or discard output sequence 
	//send only if there are messages to send and we were successful in creating them.
	TelemSendSeq_End( seqOut, rc != RC_success || ct == int: 0);
	if( rc != RC_success)
		TraceError("Cannot send sequence");
}

