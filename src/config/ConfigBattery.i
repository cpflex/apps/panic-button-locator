/**
*  Name:  ConfigBattery.p
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the battery module.
**/

/*************************
* Battery Configuration 
**************************/
#include "ui.i"
#include <OcmNidDefinitions.i>

//Define, which LED to use:
const LED_STATUS_NORMAL   = LED1; //Green
const LED_STATUS_CRITICAL = LED2; //Red

//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.
const NIDMAP: {
	NM_CHARGING			= _: NID_PowerChargerCharging,
	NM_CRITICAL			= _: NID_PowerChargerCritical,
	NM_DISCHARGING		= _: NID_PowerChargerDischarging,
	NM_CHARGECOMPLETE	= _: NID_PowerChargerCharged,
	NM_CMD_POWER_STATUS = _: NID_PowerCharger,
	NM_CMD_BATTERY_LEVEL = _: NID_PowerBattery
};

//Threshold Configurations 
const  THRESHOLD_CHARGED   = 100;			//% battery is considered charged.
const  THRESHOLD_POWERSAVE = 15;			//% enters power saving mode.
const  THRESHOLD_POWERSAVE_DISABLE = 25;    //% Forces exit of power saving mode.
const  THRESHOLD_CRITICAL = 5;				//% charge enters critical mode.
const  HYSTERESIS_FLUCTUATION = 10;			//% allowed fluctuation before resetting battery percentage to actual percentage.   
											// The larger the number,  the less fluctuation to the positive allowed.   
											// At 10, the reported battery will reset to the current battery level if greater than
											// last reading + 10.   This is needed to unlock the battery reporting racheting in cases, 
											// where the battery level was reading extremely low compared to nominal.
const  STATUS_REPORT_DISCHARGING = 5;       //% discharge interval to report battery status while normally discharging
const  STATUS_REPORT_CRITICAL = 2;          //% discharge interval to report battery status when critical.


//Monitoring interval configurations
const  INTERVAL_MONITOR_CHARGING = 10;		 //interval in seconds.
const  INTERVAL_MONITOR_DISCHARGING = 43200; //interval in seconds.
const  INTERVAL_MONITOR_POWERSAVE = 21600;	 //interval in seconds.
const  INTERVAL_MONITOR_CRITICAL  = 7200;	 //interval in seconds.

//Charging indication configurations
const  INDICATE_CHARGING_COUNT = 1;		//Number of cycles to blink.
const  INDICATE_CHARGING_ON   = 1000;	//Blink on-time in milliseconds.
const  INDICATE_CHARGING_OFF  = 1;		//Blink off-time in milliseconds.

//Charged indication configurations
const  INDICATE_CHARGED_COUNT = 1;		//Number of cycles to blink.
const  INDICATE_CHARGED_ON   = 1000;	//Blink on-time in milliseconds.
const  INDICATE_CHARGED_OFF  = 1;		//Blink off-time in milliseconds.

//Critical indication configurations
const  INDICATE_CRITICAL_COUNT = 1;		//Number of cycles to blink.
const  INDICATE_CRITICAL_ON = 300;		//Blink on-time in milliseconds.
const  INDICATE_CRITICAL_OFF = 300;		//Blink off-time in milliseconds.

//Battery % charged level indicator configurations.
const  INDICATE_CHARGE_LEVEL_ON   = 500;	//Blink on-time in milliseconds.
const  INDICATE_CHARGE_LEVEL_OFF  = 250;	//Blink off-time in milliseconds.