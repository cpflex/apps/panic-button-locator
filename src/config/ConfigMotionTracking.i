/**
*  Name:  ConfigMotionTracking.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2022-2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"
#include "telemetry.i"
#include <OcmNidDefinitions.i>


//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.

const _: {
	NM_CMD_CONFIG	= _: NID_TrackConfig,
	NM_INACTIVITY	= _: NID_TrackConfigInactivity,
	NM_NOMINTVL 	= _: NID_TrackConfigNomintvl,
	NM_EMRINTVL		= _: NID_TrackConfigEmrintvl,
	
	NM_CMD_TRACKING	= _: NID_TrackMode,
	NM_ACTIVE		= _: NID_TrackModeActive,
	NM_ACQUIRE		= _: NID_TrackConfigAcquire,
	NM_ENABLE		= _: NID_TrackModeEnabled,
	NM_DISABLE		= _: NID_TrackModeDisabled	
};


const bool:     TRACKING_INIT_ACTIVATE = false;  // If true, tracking is initially active.

// Acquire positioning data indicator

const 			TRACKING_IND_LED_ACTIVATE    = LED1;
const 			TRACKING_IND_LED_DEACTIVATE  = LED2;
const 			TRACKING_IND_COUNT = 4;
const  bool:    TRACKING_IND_ENABLE = false;		//Tracking activation / deactivation indicator is enabled.
const  bool:	ACQUIRE_IND_ENABLE	= true;		//Acquire indicator Enabled
const			ACQUIRE_IND_LED		= LED1;		//LED to use for indication.
const			ACQUIRE_IND_COUNT	= 2;		//Number of cycles to blink.
const			ACQUIRE_IND_ON		= 466;		//Blink on-time in milliseconds.
const			ACQUIRE_IND_OFF		= 133;		//Blink off-time in milliseconds.

// Persisted variable default values.
//These values define the initial value for persisted configurations.
//These are set when the application is first run after installation.
const DEFAULT_ACQUIRE = 50;		//Defines the movement threshold (mg) to begin acquisition.
const DEFAULT_INACTIVITY = 100;     //Defines movement threshold (mg) to test for innactivity
const DEFAULT_INTVL_NORMAL = 1;     //Defines the reporting interval in minutes
const DEFAULT_INTVL_EMERGENCY = 1;   //Defines the reporting interval in minutes		
const  TelemPostFlags: DEFAULT_FLAGS_POSTACQUIRE = TPF_CONFIRM_ARCHIVE;

//Comment these out to use the Location Config Module.
//const POSITIONING_MODE = PM_default;
//const POSITIONING_TECH = PT_automatic;
