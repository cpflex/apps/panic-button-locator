/**
*  Name:  ConfigSystem.p
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2022 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the battery module.
**/

/*************************
* Battery Configuration 
**************************/
#include "ui.i"
#include <OcmNidDefinitions.i>

//NID Command Map, by default these map to system protocol spec.
//If your protocol is different, update the NID mappings.
const NIDSYSMAP: {
	NM_SYS_RESET =  _: NID_SystemReset,
	NM_ARCH_ERASE = _: NID_SystemErase,
	NM_ARCH_SYSLOG = _: NID_SystemLog,
	NM_ARCH_DISABLE = _: NID_SystemLogDisable,
	NM_ARCH_ALERT = _: NID_SystemLogAlert,
	NM_ARCH_INFO = _: NID_SystemLogInfo,
	NM_ARCH_DETAIL = _: NID_SystemLogDetail,
	NM_ARCH_ALL = _: NID_SystemLogAll
};
